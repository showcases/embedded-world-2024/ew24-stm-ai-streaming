#!/bin/bash
#
# Upload packages for this job.
#

. $(dirname $0)/config.sh

# Exits on errors and allow using DEBUG=1 to trace
set -e
if [ -n "$DEBUG" ]
then
    set -x
fi

echo "Creating SD Card image tarball ..."
tar -C build-openstlinuxweston-$MACHINE/tmp-glibc/deploy/images/ \
        -c -I 'xz -9 -T0' \
        -f $(pwd)/$MACHINE-image.tar.xz \
        $MACHINE/FlashLayout_sdcard_stm32mp257f-ev1-ca35tdcid-ostl-m33-examples-optee.how_to_update.txt \
        $MACHINE/FlashLayout_sdcard_stm32mp257f-ev1-ca35tdcid-ostl-m33-examples-optee.raw

echo "Uploading SDK package ..."
$(dirname $0)/../upload-package.sh \
  $SDK_PATH/$SDK_FILE \
  sdk-${MACHINE} 0.0.1

echo "Uploading Image package ..."
$(dirname $0)/../upload-package.sh \
  $MACHINE-image.tar.xz \
  image-${MACHINE} 0.0.1
