# EW Configs
EW_STM_MANIFEST=https://github.com/STMicroelectronics/oe-manifest.git
EW_STM_BRANCH=refs/tags/openstlinux-6.6-yocto-scarthgap-mpu-v24.12.05
EW_STM_LINUX_AI=https://github.com/STMicroelectronics/meta-st-x-linux-ai
BSP_DEPENDENCY='layers/meta-st/meta-st-x-linux-ai'

# Yocto Config
DISTRO=openstlinux-weston

MACHINE=${MACHINE:-stm32mp2}
EULA_stm32mp2=1

# Extra variables
SDK_FILE="st-image-ai-openstlinux-weston-${MACHINE}.rootfs-x86_64-toolchain-5.0.3-snapshot.sh"
SDK_PATH="build-openstlinuxweston-${MACHINE}/tmp-glibc/deploy/sdk"
