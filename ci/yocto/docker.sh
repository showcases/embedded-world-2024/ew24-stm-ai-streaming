# SPDX-License-Identifier: LGPL-2.1+
#
# Copyright © 2023 Collabora Ltd
set -e

# Setup the en_US locale, because bitbake complains!
locale-gen en_US.UTF-8 && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# Get repo tool
export REPO=$(mktemp /tmp/repo.XXXXXXXXX)
curl -o ${REPO} https://storage.googleapis.com/git-repo-downloads/repo
gpg --recv-key 8BB9AD793E8E6153AF0F9A4416530D5E920F5C65
curl -s https://storage.googleapis.com/git-repo-downloads/repo.asc | gpg --verify - ${REPO}
install -m 755 ${REPO} /usr/local/bin/repo
rm -f ${REPO}
