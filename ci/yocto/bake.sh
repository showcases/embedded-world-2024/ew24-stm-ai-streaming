#!/bin/bash
#
# Build image for the Embedded World 2024 STM AI Streaming demo
#
#
script_dir=$(realpath $(dirname $0))
. $script_dir/config.sh

# Exits on errors and allow using DEBUG=1 to trace
set -e
if [ -n "$DEBUG" ]
then
    set -x
fi

# ST script will remove our env
machine=$MACHINE
source layers/meta-st/scripts/envsetup.sh --no-ui

# Apply our configuaration
cat $script_dir/local.conf >> conf/local.conf

# Build the AI image
bitbake -q st-image-ai
bitbake -q st-image-ai -c populate_sdk

# Generate an SD Card image that is ready to flash
./tmp-glibc/deploy/images/$machine/scripts/create_sdcard_from_flashlayout.sh \
  ./tmp-glibc/deploy/images/$machine/flashlayout_st-image-ai/optee/FlashLayout_sdcard_stm32mp257f-ev1-ca35tdcid-ostl-m33-examples-optee.tsv

echo =========================
echo AI image built.
echo =========================
