#!/bin/bash
#
# Download the yocto tree
#

. $(dirname $0)/config.sh

# Exits on errors and allow using DEBUG=1 to trace
set -e
if [ -n "$DEBUG" ]
then
    set -x
fi

# Cleanup
rm -rf .repo
rm -rf layers

# Some git configuration
if [ -z "$(git config --global --get user.email)" ]
then
    git config --global user.email "info@collabora.com"
    git config --global user.name "CI Yocto Builder"
fi

# Download
repo init -u $EW_STM_MANIFEST -b $EW_STM_BRANCH < /dev/null
repo sync

pushd layers/meta-st
git clone $EW_STM_LINUX_AI
popd

source layers/meta-st/scripts/envsetup.sh --no-ui

echo =========================
echo Done fetching yocto tree.
echo ==========================
