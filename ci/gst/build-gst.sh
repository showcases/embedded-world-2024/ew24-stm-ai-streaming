#!/bin/bash
set -e
if [ -n "$DEBUG" ]
then
  set -x
fi

export PATH=$HOME/.cargo/bin:$PATH
rustup target add aarch64-unknown-linux-gnu

MESON=${MESON:-/usr/bin/meson}
sdk_dir=${sdk_dir:-$PWD/sdk}
gst_dir=${gst_dir:-$PWD/gstreamer}
pluginsrs_dir=${pluginsrs_dir:-$PWD/gst-plugins-rs}
script_dir=$(realpath $(dirname $0))
overlay_root=${overlay_root:-$PWD/overlay_root}
prefix=/usr/local

mkdir -p $overlay_root

pushd $gst_dir

maybe_reconf=
if [ -d build ]
then
  maybe_reconf="--reconfigure"
fi

. $sdk_dir/environment-setup-cortexa35-ostl-linux

# The PKG_CONFIG_PATH is bogus and contains full path, drop the sysroot for now.
saved_sysroot=$PKG_CONFIG_SYSROOT_DIR
unset PKG_CONFIG_SYSROOT_DIR

cat >rust.cross <<EOF
[binaries]
rust = ['rustc', '--target', 'aarch64-unknown-linux-gnu', '-C', 'link-arg=--sysroot=$SDKTARGETSYSROOT']
rust_ld = 'aarch64-ostl-linux-gcc'
EOF

$MESON setup build \
       $maybe_reconf \
          --prefix=$prefix \
          --cross-file $sdk_dir/sysroots/x86_64-ostl_sdk-linux/usr/share/meson/aarch64-ostl-linux-meson.cross \
          --cross-file rust.cross \
          --default-library=static \
          -Dauto_features=disabled \
          -Dgstreamer:check=enabled \
          -Ddoc=disabled \
          -Dexamples=enabled \
          -Dtests=enabled \
          -Dtools=enabled \
          -Dbase=enabled \
          -Dbad=enabled \
          -Dugly=disabled \
          -Dlibav=disabled \
          -Drtsp_server=enabled \
          -Dges=disabled \
          -Drs=enabled \
          -Dgst-plugins-bad:analyticsoverlay=enabled \
          -Dgst-plugins-bad:debugutils=enabled \
          -Dgst-plugins-bad:gtk3=enabled \
          -Dgst-plugins-bad:ivfparse=enabled \
          -Dgst-plugins-bad:tflite=enabled \
          -Dgst-plugins-bad:unixfd=enabled \
          -Dgst-plugins-bad:v4l2codecs=enabled \
          -Dgst-plugins-bad:videoparsers=enabled \
          -Dgst-plugins-bad:wayland=enabled \
          -Dgst-plugins-base:gl=enabled \
          -Dgst-plugins-base:gl_api=gles2 \
          -Dgst-plugins-base:gl_platform=egl \
          -Dgst-plugins-base:gl_winsys=wayland,egl,surfaceless,gbm \
          -Dgst-plugins-base:app=enabled \
          -Dgst-plugins-base:videoconvertscale=enabled \
          -Dgst-plugins-base:typefind=enabled \
          -Dgst-plugins-base:pango=enabled \
          -Dgst-plugins-base:playback=enabled \
          -Dgst-plugins-base:videotestsrc=enabled \
          -Dgst-plugins-good:cairo=enabled \
          -Dgst-plugins-good:matroska=enabled \
          -Dgst-plugins-good:rtp=enabled \
          -Dgst-plugins-good:rtsp=enabled \
          -Dgst-plugins-good:rtpmanager=enabled \
          -Dgst-plugins-good:udp=enabled \
          -Dgst-plugins-good:v4l2=enabled \
          -Dgst-plugins-rs:originalbuffer=enabled \
          -Dgst-plugins-rs:onvif=enabled \
          -Dgst-plugins-rs:relationmeta=enabled \
          -Dgst-rtsp-server:examples=enabled

$MESON compile -C build
$MESON install -C build --destdir=$SDKTARGETSYSROOT
$MESON install -C build --strip --destdir=$overlay_root

popd

pushd program
export PKG_CONFIG_SYSROOT_DIR=$saved_sysroot
export PKG_CONFIG_PATH=$SDKTARGETSYSROOT/$prefix/lib/pkgconfig:$SDKTARGETSYSROOT/$prefix/lib/gstreamer-1.0/pkgconfig:$PKG_CONFIG_PATH
make
make DESTDIR=$overlay_root PREFIX=$prefix install
popd

# Cleanup
find $overlay_root -name "*.a" | xargs rm
find $overlay_root -name "include" | xargs rm -r
find $overlay_root -name "pkgconfig" | xargs rm -r
