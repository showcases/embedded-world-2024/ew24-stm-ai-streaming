#!/bin/bash
set -e -x

. $(dirname $0)/../yocto/config.sh

# To be done inside the CI only
if [ -n "$CI_JOB_TOKEN" ]
then
    git config --global credential.helper store
    echo https://gitlab-ci-token:$CI_JOB_TOKEN@gitlab.collabora.com > ~/.git-credentials

    if [ -f "$SDK_PATH/$SDK_FILE" ]
    then
        mv "$SDK_PATH/$SDK_FILE" sdk.sh
    else
        curl -o sdk.sh -H "JOB-TOKEN: $CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/sdk-stm32mp2/0.0.1/$SDK_FILE"
    fi

    sh sdk.sh -y -d $PWD/sdk

    # Workaround location of flatbuffers headers
    pushd sdk/sysroots/cortexa35-ostl-linux/usr/include
        ln -sf tensorflow/lite/flatbuffers/include/flatbuffers .
    popd

    rustup-init -y
fi

# Clone our modifier GStreamer/gst-plugins-rs
rm -rf gstreamer
git clone https://gitlab.collabora.com/showcases/embedded-world-2024/gstreamer.git -b 1.24-ew24-demo --depth 1
git -C gstreamer/subprojects/ clone https://gitlab.collabora.com/showcases/embedded-world-2024/gst-plugins-rs.git -b ew24-onvif --depth 1
