#!/bin/bash
#
# Upload packages for this job.
#
set -e
if [ -n "$DEBUG" ]
then
    set -x
fi

name=ew24-ai-streaming-overlay

echo "Uploading $name package ..."
tar -cJf $name.tar.xz overlay_root
$(dirname $0)/../upload-package.sh $name.tar.xz $name 0.0.1
