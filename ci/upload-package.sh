#!/bin/bash
set -e
if [ -n "$DEBUG" ]
then
    set -x
fi

if [ -z "$CI_JOB_TOKEN" ]
then
    echo "ERROR: CI_JOB_TOKEN must be set to use this script."
    exit 1
fi

if [ -z "$CI_API_V4_URL" ]
then
    echo "ERROR: CI_API_V4_URL must be set to use this script."
    exit 1
fi

filepath=$1
filename=$(basename $filepath)
name=$2
version=$3

curl --upload-file $filepath -H "JOB-TOKEN: $CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/$name/$version/$filename"
