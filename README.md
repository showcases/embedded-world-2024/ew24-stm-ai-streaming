# STM32MP2 demo for Embedded World 2024

# Overview

This demo implements AI wild life detection and tracking. The general idea is
that a device in the field will be monitoring wild life. The resulting video
and AI generated metadata can then be served over the network using RTSP
and ONVIF standard for analytic metadata. The demo also includes a receiver
application that act as an RSTP Onvif client and render the video and the
metadata on screen.

This demo makes use of various recently added feature to GStreamer and Linux:

- Hardware Stateless H.264 encoder in Hantro V4L2 driver and GStreamer
- TFLite inference
- GStreamer Analytic Metadata
- Onvif translators from/to GStreamer Analytic Meta
- Unix FD IPC element.

### Board setup

The first step is to provision the board with a working image. For this, you
will have to download the appropriate package for your board. If you run a 
production board the
[sdk-stm32mp2](https://gitlab.collabora.com/showcases/embedded-world-2024/ew24-stm-ai-streaming/-/packages/259)
package is what you need. Visit this link and click on the most recent upload to
download the latest. The package contains two files, an instruction file and
a `.raw`. You may follow the instructions or simply use the `Restore from image`
feature of Gnome Disk if you like. You will have to flash an SD card from this image.

You will need to add the demo application software. Download the lastest
[ew24-ai-streaming-overlay](https://gitlab.collabora.com/showcases/embedded-world-2024/ew24-stm-ai-streaming/-/packages/253)
package and untar it. The content needs to be copied into the `userfs`
partition. Here's a short script you may adapt to actually modify the image in-place.

```
tar -xvf stm32mp2-image.tar.xz
tar -xvf ew24-ai-streaming-overlay.tar.xz
udisksctl loop-setup -f stm32mp2*/*.raw
sudo cp -r overlay_root/usr/local/* /run/media/$(id -nu)/userfs/
sudo echo "stm32mp25-server" >
udisksctl unmount -b /dev/loop0p8
udisksctl unmount -b /dev/loop0p9
udisksctl unmount -b /dev/loop0p10
udisksctl unmount -b /dev/loop0p11
dd if=stm32mp2/*.raw of=/dev/sda bs=8M
```

#### Resetting the MAC address

If multiple boards end up with the same MAC address, the following procedure
can reset it.

1. Connect to the STLink USB Type C port on the board to a computer
2. Connect via serial: picocom -b 115200 /dev/ttyACM0
3. Interrupt the boot into uboo
4. U-boot $> env default -a
5. U-boot $> env save
6. U-boot $> reset

It should now get a new MAC address

### Manual image compilation

```
./ci/prepare.sh
./ci/bake.sh
```

After a long wait, this will create an SDK and an image to flash you board. For software
development, use the generated SDK. You can seek inspiration by looking at scripts in
`ci/gst/`. You can also download an SDK from the CI
[SDK](https://gitlab.collabora.com/showcases/embedded-world-2024/ew24-stm-ai-streaming/-/packages/258)
package.
