/*
 * Copyright (C) 2014-2015 Collabora Ltd.
 *   @author George Kiagiadakis <george.kiagiadakis@collabora.com>
 * Copyright (C) 2019, STMicroelectronics - All Rights Reserved
 *   @author Christophe Priouzeau <christophe.priouzeau@st.com>
 *
 * SPDX-License-Identufier: GPL-2.0+
 */

/* Compile with:
 * gcc -o server server.c $(pkg-config --libs --cflags gstreamer-1.0 gtk+-3.0)
 */

#include <gdk/gdk.h>
#include <gst/gst.h>
#include <gst/rtsp-server/rtsp-server.h>
#include <gtk/gtk.h>

#define DEFAULT_MODEL_FILE "/usr/local/demo-ai/wildlife/wildlife_model.tflite"
#define DEFAULT_LABELS "/usr/local/demo-ai/wildlife/wildlife_classes.txt"
#define DEFAULT_DELEGATE_SO "libvx_delegate.so.2"
#define DEFAULT_INTERVAL "200000000"
#define DEFAULT_SCORE_THRESHOLD "0.45"
#define DEFAULT_SIZE_THRESHOLD "0.8"
#define DEFAULT_BITRATE 10000000

#define DEFAULT_RTSP_PORT "8554"
#define DEFAULT_RTSP_MOUNT "/ai-onvif"

#define VIDEO_SOCKET "socket-type=abstract socket-path=rtsp-ai-onvif-video"
#define META_SOCKET "socket-type=abstract socket-path=rtsp-ai-onvif-metadata"

#ifdef __aarch64__
#define SERVER_PIPELINE "unixfdsrc " VIDEO_SOCKET " ! v4l2slh264enc name=enc rate-control=cbr ! identity name=rate-monitor ! rtph264pay name=pay0 " \
                        "unixfdsrc " META_SOCKET " ! rtponvifmetadatapay name=pay1 "
#define APP_PIPELINE "libcamerasrc src::stream-role=view-finder ! video/x-raw,width=640,height=480,format=RGB16,framerate=29/1 ! tee name=t " \
                     "t. ! queue name=wlqueue ! gtkwaylandsink sync=false async=false " \
  "t. ! queue name=tflitequeue ! originalbuffersave name=osave ! videoconvertscale ! video/x-raw,format=RGB " \
                        "! tfliteinference name=inf ! ssdobjectdetector name=detect "\
                        "! originalbufferrestore ! relationmeta2onvifmeta ! onvifmetadataseparator name=s " \
                        "s.meta_src ! queue name=metaqueue ! unixfdsink allow-copy=1 async=0 name=meta-ipc " META_SOCKET " " \
                        "s.media_src ! queue name=mediaqueue ! unixfdsink async=0 name=video-ipc " VIDEO_SOCKET
#else
#define SERVER_PIPELINE "unixfdsrc " VIDEO_SOCKET " ! openh264enc rate-control=bitrate ! video/x-h264,profile=main ! rtph264pay name=pay0 "
#define APP_PIPELINE "videotestsrc pattern=18 background-color=0x000062FF ! video/x-raw,width=640,height=480,format=BGRx ! tee name=t " \
                     "t. ! queue ! videoconvert ! gtkwaylandsink " \
                     "t. ! queue ! videoconvert ! video/x-raw,format=I420 ! unixfdsink " VIDEO_SOCKET
#endif

typedef struct
{
  // Visual
  GtkWidget *window_widget;
  GtkWidget *label; // owned by the window
  GMainLoop *loop;

  // App pipeline
  GstElement *pipeline;
  guint bus_watch_id;

  // RTSP Server
  GstRTSPServer *server;
  guint server_source_id;
  GWeakRef encoder_ref;
  const char *port;
  const char *mount;

  // AI Configuration
  const char *model_file;
  const char *labels;
  const char *delegate_so;
  const char *interval;
  const char *score;
  const char *size;
  gint bitrate;

  // Stats
  GWeakRef monitor_ref;
  guint64 rate_ts;
  guint64 bytes_total;
  guint64 inf_time;
  guint64 inf_total;
} DemoApp;

  static void
msg_state_changed (GstBus * bus, GstMessage * message, gpointer user_data)
{
  const GstStructure *s;
  DemoApp *d = user_data;

  s = gst_message_get_structure (message);

  /* We only care about state changed on the pipeline */
  if (s && GST_MESSAGE_SRC (message) == GST_OBJECT_CAST (d->pipeline)) {
    GstState old, new, pending;

    gst_message_parse_state_changed (message, &old, &new, &pending);

    switch (new){
      case GST_STATE_VOID_PENDING:
        g_debug ("new state: GST_STATE_VOID_PENDING\n");
        break;
      case GST_STATE_NULL:
        g_debug ("new state: GST_STATE_NULL\n");
        break;
      case GST_STATE_READY:
        g_debug ("new state: GST_STATE_READY\n");
        break;
      case GST_STATE_PAUSED:
        g_debug ("new state: GST_STATE_PAUSED\n");
        break;
      case GST_STATE_PLAYING:
        g_debug ("new state: GST_STATE_PLAYING\n");
        break;
      default:
        break;
    }
  }
}

static gboolean
gstreamer_bus_callback (GstBus * bus, GstMessage * message, void *data)
{
  DemoApp *d = data;

  g_debug ("Got %s message\n", GST_MESSAGE_TYPE_NAME (message));

  switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_ERROR: {
      g_autoptr(GError) err = NULL;
      g_autofree gchar *debug;

      g_print ("Got %s message\n", GST_MESSAGE_TYPE_NAME (message));

      gst_message_parse_error (message, &err, &debug);
      g_print ("Error: %s\n", err->message);
      if (debug)
        g_print ("Debug details: %s\n", debug);
      g_main_loop_quit (d->loop);
      break;
    }

    case GST_MESSAGE_STATE_CHANGED:
      msg_state_changed (bus, message, data);
      break;

    case GST_MESSAGE_EOS:
      /* end-of-stream */
      g_print ("EOS\n");
      g_main_loop_quit (d->loop);
      break;

    default:
      /* unhandled message */
      break;
  }

  return TRUE;
}

static gboolean
dump_debug (gpointer user_data)
{
  GstObject *pipe;
  GWeakRef *ref = user_data;
  GstElement *e = g_weak_ref_get (ref);
  if (!e)
    return G_SOURCE_REMOVE;
  pipe = gst_element_get_parent (GST_ELEMENT (e));
  g_debug ("Dumping pipeline, use GST_DEBUG_DOT_DIR env to enabled.");
  GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (pipe), GST_DEBUG_GRAPH_SHOW_ALL,
      "rtsp-server-test-launch");
  return G_SOURCE_CONTINUE;
}

static gboolean
update_stats (gpointer user_data)
{
  DemoApp *d = user_data;
  GstStructure *s = NULL;
  g_autofree gchar *rate_str = g_strdup ("--.-- Mb/s");
  g_autofree gchar *inf_str = g_strdup ("--- ms/Inf.");

  g_autoptr (GstElement) e = g_weak_ref_get (&d->monitor_ref);
  if (e) {
    guint64 now, bytes_total;
    gdouble duration, num_mbits, rate;

    g_object_get (e, "stats", &s, NULL);
    now = g_get_monotonic_time () * G_GUINT64_CONSTANT (1000);

    if (s == NULL) {
      g_warning ("NULL stats in stats property.");
      return G_SOURCE_CONTINUE;
    }

    if (!gst_structure_get_uint64 (s, "num-bytes", &bytes_total)) {
      g_warning ("No num-bytes field in stats.");
      return G_SOURCE_CONTINUE;
    }

    duration = (gdouble)(now - d->rate_ts) / (gdouble) GST_SECOND;
    num_mbits = (gdouble)(bytes_total - d->bytes_total) * 8.0 / (gdouble) (1000000);
    rate = num_mbits / duration;

    g_free (rate_str);
    rate_str = g_strdup_printf ("%.2f Mb/s", rate);

    d->rate_ts = now;
    d->bytes_total = bytes_total;
    g_clear_object (&e);
  }

  e = gst_bin_get_by_name (GST_BIN (d->pipeline), "inf");
  if (e) {
    guint64 inf_time, inf_total, duration, num_inf;

    g_object_get (e, "stats", &s, NULL);

    if (s == NULL) {
      g_warning ("NULL stats in stats property.");
      return G_SOURCE_CONTINUE;
    }

    if (!gst_structure_get_uint64 (s, "num-buffers", &inf_total)) {
      g_warning ("No num-buffers field in stats.");
      return G_SOURCE_CONTINUE;
    }

    if (!gst_structure_get_uint64 (s, "inf-time", &inf_time)) {
      g_warning ("No inf-time field in stats.");
      return G_SOURCE_CONTINUE;
    }

    num_inf = inf_total - d->inf_total;
    duration = inf_time - d->inf_time;

    if (num_inf) {
      guint64 inf_time = duration / num_inf / GST_MSECOND;
      g_free (inf_str);
      inf_str = g_strdup_printf ("%" G_GUINT64_FORMAT " ms/Inf.", inf_time);
    }

    d->inf_time = inf_time;
    d->inf_total = inf_total;
    g_clear_object (&e);
  }

  g_autofree gchar *text = g_strdup_printf ("%s\n%s\n%s", "Codec H.264", rate_str, inf_str);
  gtk_label_set_text (GTK_LABEL (d->label), text);

  return G_SOURCE_CONTINUE;
}

static void
constructed (GstRTSPMediaFactory * self G_GNUC_UNUSED, GstRTSPMedia * m,
    gpointer user_data)
{
  DemoApp *d = user_data;
  g_autoptr (GstElement) e = gst_rtsp_media_get_element (m);

  GWeakRef *ref = g_new0 (GWeakRef, 1);
  g_weak_ref_set (ref, e);
  dump_debug (ref);
  g_timeout_add_seconds (5, dump_debug, ref);

  g_autoptr (GstElement) monitor = gst_bin_get_by_name (GST_BIN (e), "rate-monitor");
  d->rate_ts = g_get_monotonic_time ();
  d->bytes_total = 0;
  g_weak_ref_set (&d->monitor_ref, monitor);

  g_autoptr (GstElement) encoder = gst_bin_get_by_name (GST_BIN (e), "enc");
  g_object_set (encoder, "bitrate", d->bitrate, NULL);
  g_weak_ref_set (&d->encoder_ref, encoder);
}

static gboolean
build_pipeline (DemoApp *d, GError **error)
{
  d->pipeline = gst_parse_launch (APP_PIPELINE, error);
  if (*error)
    return FALSE;

#ifdef __aarch64__
  g_autoptr (GstElement) inf = gst_bin_get_by_name (GST_BIN (d->pipeline), "inf");
  gst_util_set_object_arg (G_OBJECT (inf), "execution-provider", "npu");
  gst_util_set_object_arg (G_OBJECT (inf), "ext-delegate", d->delegate_so);
  gst_util_set_object_arg (G_OBJECT (inf), "model-file", d->model_file );

  g_autoptr (GstElement) detect = gst_bin_get_by_name (GST_BIN (d->pipeline), "detect");
  gst_util_set_object_arg (G_OBJECT (detect), "label-file", d->labels);
  gst_util_set_object_arg (G_OBJECT (detect), "score-threshold", d->score);
  gst_util_set_object_arg (G_OBJECT (detect), "size-threshold", d->size);

  g_autoptr (GstElement) osave = gst_bin_get_by_name (GST_BIN (d->pipeline), "osave");
  gst_util_set_object_arg (G_OBJECT (osave), "interval", d->interval);
#endif

  g_autoptr(GstBus) bus = gst_pipeline_get_bus (GST_PIPELINE (d->pipeline));
  d->bus_watch_id = gst_bus_add_watch (bus, gstreamer_bus_callback, d);

  d->server = gst_rtsp_server_new ();
  g_object_set (d->server, "service", d->port, NULL);

  g_autoptr(GstRTSPMountPoints) mounts = gst_rtsp_server_get_mount_points (d->server);
  GstRTSPMediaFactory *factory = gst_rtsp_media_factory_new ();

  gst_rtsp_media_factory_set_launch (factory, SERVER_PIPELINE);
  gst_rtsp_media_factory_set_shared (factory, TRUE);
  gst_rtsp_media_factory_set_enable_rtcp (factory, TRUE);
  gst_rtsp_media_factory_set_profiles (factory, GST_RTSP_PROFILE_AVPF);
  gst_rtsp_media_factory_set_suspend_mode (factory, GST_RTSP_SUSPEND_MODE_NONE);
  gst_rtsp_media_factory_set_do_retransmission (factory, TRUE);

  g_signal_connect (factory, "media-constructed", G_CALLBACK (constructed), d);
  gst_rtsp_mount_points_add_factory (mounts, d->mount, g_steal_pointer (&factory));

  d->server_source_id = gst_rtsp_server_attach (d->server, NULL);
  g_print ("stream ready at rtsp://127.0.0.1:%s%s\n", d->port, d->mount);

  return TRUE;
}

static gchar*
bitrate_changed (GtkScale *scale, gdouble value, DemoApp *d)
{
  d->bitrate = (int) (value * 1000000);
  g_autoptr (GstElement) e = g_weak_ref_get (&d->encoder_ref);
  if (e)
    g_object_set (e, "bitrate", d->bitrate, NULL);

  return g_strdup_printf ("%u Mb/s", d->bitrate / 1000000);
}

static void
build_window (DemoApp *d)
{
  GtkCssProvider* provider;
  GtkWidget *video_widget = NULL;
  GtkWidget *box, *box2, *box3, *button, *slider, *image;

  /* windows */
  d->window_widget = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW(d->window_widget), "GStreamer AI ONVIF RTSP Server ");
  gtk_window_fullscreen(GTK_WINDOW(d->window_widget));
  gtk_window_set_decorated (GTK_WINDOW(d->window_widget), FALSE);

  g_signal_connect (d->window_widget, "destroy",
      G_CALLBACK (gtk_widget_destroyed), &d->window_widget);
  g_signal_connect_swapped (d->window_widget, "destroy",
      G_CALLBACK (g_main_loop_quit), d->loop);

  box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_container_add(GTK_CONTAINER (d->window_widget), box);

  box2 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_box_pack_end (GTK_BOX (box), box2, FALSE, TRUE, 0);

  box3 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_pack_end (GTK_BOX (box2), box3, FALSE, TRUE, 0);

  slider = gtk_scale_new_with_range (GTK_ORIENTATION_VERTICAL, 1, 20, 1);
  gdouble default_bitrate = (gdouble) d->bitrate / 1000000.0;
  gtk_scale_add_mark (GTK_SCALE (slider), default_bitrate, GTK_POS_LEFT, NULL);
  gtk_range_set_value (GTK_RANGE (slider), default_bitrate);
  gtk_range_set_inverted (GTK_RANGE (slider), TRUE);
  g_signal_connect (slider, "format-value", G_CALLBACK (bitrate_changed), d);

  image = gtk_image_new_from_file("/usr/local/share/COLLABORA_02_RGB.png");
  d->label = gtk_label_new ("");

  PangoAttrList *attrlist = pango_attr_list_new();
  PangoFontDescription * font_desc = pango_font_description_new();
  pango_font_description_set_size(font_desc, 18 * PANGO_SCALE);
  PangoAttribute * attr = pango_attr_font_desc_new(font_desc);
  pango_attr_list_insert(attrlist, attr);
  gtk_label_set_attributes(GTK_LABEL (d->label), attrlist);

  update_stats (d);
  g_timeout_add_seconds (1, update_stats, d);

  button = gtk_button_new_from_icon_name("window-close-symbolic", GTK_ICON_SIZE_DIALOG);
  g_signal_connect_swapped (button, "clicked",
      G_CALLBACK (g_main_loop_quit), d->loop);

  gtk_widget_set_size_request (button, 0, 140);
  g_object_set (button, "margin-top", 20, "margin-bottom", 20,
      "margin-start", 20, "margin-end", 20, NULL);
  g_object_set (image, "margin-top", 20, "margin-bottom", 20,
      "margin-start", 20, "margin-end", 20, NULL);

  gtk_box_pack_start (GTK_BOX(box2), button, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box2), d->label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(box3), slider, TRUE, TRUE, 0);
  gtk_box_pack_end (GTK_BOX(box3), image, TRUE, TRUE, 0);

  /* styling background color to black */
  const char *data = "#transparent_bg,GtkDrawingArea {\n"
    "    background-color: rgba (88%, 88%, 88%, 1.0);\n"
    "}";

  provider = gtk_css_provider_new();
  gtk_css_provider_load_from_data(provider, data, -1, NULL);
  gtk_style_context_add_provider(gtk_widget_get_style_context(d->window_widget),
      GTK_STYLE_PROVIDER(provider),
      GTK_STYLE_PROVIDER_PRIORITY_USER);
  g_object_unref(provider);

  gtk_widget_set_name(d->window_widget, "transparent_bg");

  GstIterator *it = gst_bin_iterate_all_by_element_factory_name (GST_BIN (d->pipeline),
      "gtkwaylandsink");
  GValue v = {0};
  g_value_init(&v, GST_TYPE_ELEMENT);
  GstIteratorResult res = gst_iterator_next (it, &v);
  g_assert (res == GST_ITERATOR_OK);
  gst_iterator_free (it);

  g_object_get (g_value_get_object (&v), "widget", &video_widget, NULL);
  gtk_widget_set_support_multidevice (video_widget, TRUE);
  gtk_widget_set_vexpand (video_widget, TRUE);

  gtk_box_pack_start (GTK_BOX (box), video_widget, TRUE, TRUE, 0);

  gtk_widget_show_all (d->window_widget);

  g_object_unref (video_widget);
  g_value_unset (&v);
}

static void
terminate_application (DemoApp *d)
{
  if (d->bus_watch_id)
    g_source_remove (d->bus_watch_id);

  if (d->server_source_id)
    g_source_remove (d->server_source_id);

  if (d->server)
    gst_object_unref (d->server);

  if (d->pipeline)
    gst_object_unref (d->pipeline);

  if (d->window_widget)
    gtk_widget_destroy (d->window_widget);

  g_print ("Application terminated.\n");
  *d = (DemoApp) {NULL,};
}

int
main (int argc, char **argv)
{
  DemoApp app = {
    .port = DEFAULT_RTSP_PORT,
    .mount = DEFAULT_RTSP_MOUNT,
    .model_file = DEFAULT_MODEL_FILE,
    .labels = DEFAULT_LABELS,
    .delegate_so = DEFAULT_DELEGATE_SO,
    .interval = DEFAULT_INTERVAL,
    .score = DEFAULT_SCORE_THRESHOLD,
    .size = DEFAULT_SIZE_THRESHOLD,
    .bitrate = DEFAULT_BITRATE,
  };
  GOptionEntry entries[] = {
    {"port", 'p', 0, G_OPTION_ARG_STRING, &app.port,
      "Port to listen on (default: " DEFAULT_RTSP_PORT ")", "PORT"},
    {"mount", 'm', 0, G_OPTION_ARG_STRING, &app.mount,
      "Mountpoint (default: " DEFAULT_RTSP_MOUNT ")", "MOUNT"},
    {"model-file", 'f', 0, G_OPTION_ARG_STRING, &app.model_file,
      "TFLite model file (default: " DEFAULT_MODEL_FILE ")", "MODEL"},
    {"labels", ';', 0, G_OPTION_ARG_STRING, &app.labels,
      "TFLite model labels (default: " DEFAULT_LABELS ")", "LABELS"},
    {"delegate_so", ';', 0, G_OPTION_ARG_STRING, &app.delegate_so,
      "TFLite deletegate library (default: " DEFAULT_DELEGATE_SO ")", "DELEGATE"},
    {"interval", 'i', 0, G_OPTION_ARG_STRING, &app.interval,
      "Max inference distance (default: " DEFAULT_INTERVAL ")", "INTERVAL"},
    {"score-threshold", 's', 0, G_OPTION_ARG_STRING, &app.score,
      "Minimum object score (default: " DEFAULT_SCORE_THRESHOLD ")", "SCORE"},
    {"size-threshold", 'z', 0, G_OPTION_ARG_STRING, &app.size,
      "Maxium object sreen coverage (default: " DEFAULT_SIZE_THRESHOLD ")", "SIZE"},
    {"bitrate", 'b', 0, G_OPTION_ARG_INT, &app.bitrate,
      "Encoder bitrate (default: " G_STRINGIFY (DEFAULT_BITRATE) ")", "BITRATE"},
    {NULL}
  };

  g_autoptr(GOptionContext) context;
  g_autoptr(GError) error = NULL;
  int ret = 0;

  context = g_option_context_new ("- ONVIF AI RTSP Server");
  g_option_context_add_group (context, gtk_get_option_group (TRUE));
  g_option_context_add_group (context, gst_init_get_option_group ());
  g_option_context_add_main_entries (context, entries, NULL);
  if (!g_option_context_parse (context, &argc, &argv, &error)) {
    goto out;
  }

  if (app.bitrate < 0)
    app.bitrate = 0;

  app.loop = g_main_loop_new (NULL, FALSE);

  if (!build_pipeline (&app, &error))
    goto out;

  build_window (&app);

  gst_element_set_state (app.pipeline, GST_STATE_PLAYING);
  g_main_loop_run (app.loop);
  gst_element_set_state (app.pipeline, GST_STATE_NULL);

out:
  if (error) {
    g_printerr ("ERROR: %s\n", error->message);
    ret = 1;
  }

  terminate_application (&app);

  return ret;
}
