/*
 * Copyright (C) 2014-2015 Collabora Ltd.
 *   @author George Kiagiadakis <george.kiagiadakis@collabora.com>
 * Copyright (C) 2019, STMicroelectronics - All Rights Reserved
 *   @author Christophe Priouzeau <christophe.priouzeau@st.com>
 *
 * SPDX-License-Identufier: GPL-2.0+
 */

/* Compile with:
 * gcc -o receiver receiver.c $(pkg-config --libs --cflags gstreamer-1.0 gtk+-3.0)
 */

#include <gst/gst.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>

static gchar *graph = NULL;
static gchar *shader_file = NULL;
static gboolean nofullscreen = FALSE;

static GOptionEntry entries[] = {
	{"nofullscreen", 'F', 0, G_OPTION_ARG_NONE, &nofullscreen,
		"Do not put video on fullscreeen", NULL},
	{"graph", 0, 0, G_OPTION_ARG_STRING, &graph, "Gstreamer graph to use", NULL},

	{NULL}
};

typedef struct
{
	GtkWidget *window_widget;

	GstElement *pipeline;

	GMainLoop *loop;
	guint io_watch_id;

	gchar **argv;
	gint current_uri;             /* index for argv */
} DemoApp;

static void
on_about_to_finish (GstElement * playbin, DemoApp * d)
{
	if (d->argv[++d->current_uri] == NULL)
		d->current_uri = 1;

	g_print ("Now playing %s\n", d->argv[d->current_uri]);
	g_object_set (playbin, "uri", d->argv[d->current_uri], NULL);
}

static void
msg_state_changed (GstBus * bus, GstMessage * message, gpointer user_data)
{
	const GstStructure *s;
	DemoApp *d = user_data;

	s = gst_message_get_structure (message);

	/* We only care about state changed on the pipeline */
	if (s && GST_MESSAGE_SRC (message) == GST_OBJECT_CAST (d->pipeline)) {
		GstState old, new, pending;

		gst_message_parse_state_changed (message, &old, &new, &pending);

		switch (new){
		case GST_STATE_VOID_PENDING:
			g_print("new state: GST_STATE_VOID_PENDING\n");
			break;
		case GST_STATE_NULL:
			g_print("new state: GST_STATE_NULL\n");
			break;
		case GST_STATE_READY:
			g_print("new state: GST_STATE_READY\n");
			break;
		case GST_STATE_PAUSED:
			g_print("new state: GST_STATE_PAUSED\n");
			break;
		case GST_STATE_PLAYING:
			g_print("new state: GST_STATE_PLAYING\n");
			break;
		default:
			break;
		}
	}
}

static gboolean
gstreamer_bus_callback (GstBus * bus, GstMessage * message, void *data)
{
	DemoApp *d = data;

	//g_print ("Got %s message\n", GST_MESSAGE_TYPE_NAME (message));

	switch (GST_MESSAGE_TYPE (message)) {
	case GST_MESSAGE_ERROR:{
		GError *err;
		gchar *debug;

		g_print ("Got %s message\n", GST_MESSAGE_TYPE_NAME (message));

		gst_message_parse_error (message, &err, &debug);
		g_print ("Error:  %s %d  %s\n",
                    g_quark_to_string (err->domain), err->code, err->message);
		if (debug) {
			g_print ("Debug details: %s\n", debug);
			g_free (debug);
		}
                if (g_error_matches (err, GST_RESOURCE_ERROR,
                        GST_RESOURCE_ERROR_READ) ||
                    g_error_matches (err, GST_RESOURCE_ERROR,
                        GST_RESOURCE_ERROR_OPEN_READ_WRITE)) {
                  g_print("Got resource error, restarting\n");
                  gst_element_set_state (d->pipeline, GST_STATE_NULL);
                  gst_element_set_state (d->pipeline, GST_STATE_PLAYING);
                } else {
                  g_main_loop_quit (d->loop);
                }
                g_error_free (err);
		break;
	}
	case GST_MESSAGE_WARNING:{
		GError *err;
		gchar *debug;

		g_print ("Got %s message\n", GST_MESSAGE_TYPE_NAME (message));

		gst_message_parse_warning (message, &err, &debug);
		g_print ("Warning: %s %d %s\n",
                    g_quark_to_string (err->domain), err->code, err->message);
		if (debug) {
			g_print ("Debug details: %s\n", debug);
			g_free (debug);
		}

                if (g_error_matches (err, GST_RESOURCE_ERROR,
                        GST_RESOURCE_ERROR_READ)) {
                  g_print("Got resource read warning, restarting\n");
                  gst_element_set_state (d->pipeline, GST_STATE_NULL);
                  gst_element_set_state (d->pipeline, GST_STATE_PLAYING);
                }
		g_error_free (err);

                break;
	}

	case GST_MESSAGE_STATE_CHANGED:
		msg_state_changed (bus, message, data);
		break;

	case GST_MESSAGE_EOS:
		/* end-of-stream */
		g_print ("EOS\n");
		g_main_loop_quit (d->loop);
		break;

	default:
		/* unhandled message */
		break;
	}
	return TRUE;
}

static void
toggle_overlay (GtkToggleButton *button, GstElement *overlay)
{
  g_object_set(overlay, "draw", gtk_toggle_button_get_active(button), NULL);
}

static void
build_window (DemoApp * d)
{
	GtkCssProvider* provider;
	GtkWidget *video_widget = NULL;
        GtkWidget *box, *box2, *button, *image;

	/* windows */
	d->window_widget = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW(d->window_widget), "GStreamer GTK ");
	gtk_window_set_default_size (GTK_WINDOW (d->window_widget), 320, 240);
	g_signal_connect (d->window_widget, "destroy",
			G_CALLBACK (gtk_widget_destroyed), &d->window_widget);
	g_signal_connect_swapped (d->window_widget, "destroy",
			G_CALLBACK (g_main_loop_quit), d->loop);

	if (!nofullscreen)
		gtk_window_fullscreen(GTK_WINDOW(d->window_widget));
	//else {
	//	gtk_window_set_decorated (GTK_WINDOW(d->window_widget), FALSE);
	//}


        box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_container_add(GTK_CONTAINER (d->window_widget), box);

        box2 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
        gtk_box_pack_end (GTK_BOX (box), box2, FALSE, TRUE, 0);


        image = gtk_image_new_from_file("/usr/local/share/COLLABORA_02_RGB.png");

        button = gtk_button_new_from_icon_name("window-close-symbolic", GTK_ICON_SIZE_DIALOG);
	g_signal_connect_swapped (button, "clicked",
			G_CALLBACK (g_main_loop_quit), d->loop);

        gtk_widget_set_size_request (button, 0, 90);
        g_object_set (button, "margin-top", 20, "margin-bottom", 20,
            "margin-start", 20, "margin-end", 20, NULL);
        g_object_set (image, "margin-top", 20, "margin-bottom", 20,
            "margin-start", 20, "margin-end", 20, NULL);

        gtk_box_pack_start (GTK_BOX(box2), button, FALSE, TRUE, 0);
        gtk_box_pack_end (GTK_BOX(box2), image, TRUE, TRUE, 0);

        g_autoptr (GstElement) overlay = gst_bin_get_by_name(GST_BIN(d->pipeline), "overlay");
        if (overlay) {
          GtkWidget *button2, *image2;

          image2 = gtk_image_new_from_file("/usr/local/share/overlay.png");

          button2 = gtk_toggle_button_new();
          gtk_container_add (GTK_CONTAINER(button2), image2);
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button2), TRUE);
          g_signal_connect (button2, "toggled", G_CALLBACK(toggle_overlay), overlay);

          gtk_widget_set_size_request (button2, 0, 90);
          g_object_set (button2, "margin-top", 20, "margin-bottom", 20,
              "margin-start", 20, "margin-end", 20, NULL);

          gtk_box_pack_end (GTK_BOX(box2), button2, FALSE, FALSE, 0);
        }

	/* styling background color to black */
	const char *data = "#transparent_bg,GtkDrawingArea {\n"
			"    background-color: rgba (88%, 88%, 88%, 1.0);\n"
			"}";

	provider = gtk_css_provider_new();
	gtk_css_provider_load_from_data(provider, data, -1, NULL);
	gtk_style_context_add_provider(gtk_widget_get_style_context(d->window_widget),
                                   GTK_STYLE_PROVIDER(provider),
                                   GTK_STYLE_PROVIDER_PRIORITY_USER);
	g_object_unref(provider);

	gtk_widget_set_name(d->window_widget, "transparent_bg");

        GstIterator *it = gst_bin_iterate_all_by_element_factory_name (GST_BIN (d->pipeline),
            "gtkwaylandsink");
        GValue v = {0};
        g_value_init(&v, GST_TYPE_ELEMENT);
        GstIteratorResult res = gst_iterator_next (it, &v);
        g_assert (res == GST_ITERATOR_OK);
        gst_iterator_free (it);

       	g_object_get (g_value_get_object (&v), "widget", &video_widget, NULL);
	gtk_widget_set_support_multidevice (video_widget, TRUE);
	gtk_widget_set_vexpand (video_widget, TRUE);

	gtk_box_pack_start (GTK_BOX (box), video_widget, TRUE, TRUE, 0);


	gtk_widget_show_all (d->window_widget);

	g_object_unref (video_widget);
        g_value_unset (&v);
}


int
main (int argc, char **argv)
{
	DemoApp app = {NULL}, *d = &app;
	GOptionContext *context;
	GstBus *bus;
	GError *error = NULL;
	guint bus_watch_id = 0;
	int ret = 0;

	gtk_init (&argc, &argv);
	gst_init (&argc, &argv);

	context = g_option_context_new ("- waylandsink gtk demo");
	g_option_context_add_main_entries (context, entries, NULL);
	if (!g_option_context_parse (context, &argc, &argv, &error)) {
		g_option_context_free (context);
		goto out;
	}
	g_option_context_free (context);

	if (argc > 1) {
		d->argv = argv;
		d->current_uri = 1;

		d->pipeline = gst_parse_launch ("playbin video-sink=gtkwaylandsink", &error);
		if (error)
			goto out;

		g_object_set (d->pipeline, "uri", argv[d->current_uri], NULL);

		/* enable looping */
		g_signal_connect (d->pipeline, "about-to-finish",
						  G_CALLBACK (on_about_to_finish), d);
	} else {
		if (graph != NULL) {
			if (strstr(graph, "gtkwaylandsink") != NULL) {
				d->pipeline = gst_parse_launch (graph, NULL);
			} else {
				g_print("ERROR: graph does not contain gtkwaylandsink !!!\n");
				ret = 1;
				goto out;
			}
		} else {
			d->pipeline = gst_parse_launch ("videotestsrc pattern=18 "
					"background-color=0x000062FF ! glupload ! glcolorscale ! glcolorconvert ! gldownload ! gtkwaylandsink",
					&error);
			if (error)
				goto out;
		}
	}

	d->loop = g_main_loop_new (NULL, FALSE);
	build_window (d);

	bus = gst_pipeline_get_bus (GST_PIPELINE (d->pipeline));
	bus_watch_id = gst_bus_add_watch (bus, gstreamer_bus_callback, d);
	gst_object_unref (bus);

	gst_element_set_state (d->pipeline, GST_STATE_PLAYING);

	g_main_loop_run (d->loop);

	gst_element_set_state (d->pipeline, GST_STATE_NULL);

out:
	if (error) {
		g_printerr ("ERROR: %s\n", error->message);
		ret = 1;
	}
	g_clear_error (&error);

	if (bus_watch_id)
		g_source_remove (bus_watch_id);

	gst_clear_object (&d->pipeline);
	if (d->window_widget) {
		g_print ("destroy window\n");
		g_signal_handlers_disconnect_by_data (d->window_widget, d->loop);
		gtk_widget_destroy (d->window_widget);
	}
	g_clear_pointer (&d->loop, g_main_loop_unref);

	g_free(graph);
	g_free(shader_file);

	return ret;
}
