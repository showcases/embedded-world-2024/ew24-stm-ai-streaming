#!/bin/sh
export LD_LIBRARY_PATH=/usr/local/lib
/usr/local/bin/ew-ai-rtsp-client --graph "rtspsrc name=src latency=500 location=rtsp://stm32mp25-server:8554/ai-onvif add-reference-timestamp-meta=1 onvif-mode=1 ! rtph264depay ! decodebin3 ! onvifmetadatacombiner name=combi ! queue ! onvifmeta2relationmeta ! objectdetectionoverlay name=overlay linger=10 ! gtkwaylandsink src. ! rtponvifmetadatadepay !  onvifmetadataparse latency=50000000 ! combi."
