#!/bin/sh

function pty_exec() {
    pty=$(tty > /dev/null 2>&1; echo $?)
    event_cmd="/usr/local/bin/ew-ai-rtsp-server"
    if [ $pty -eq 0 ]; then
        eval $event_cmd > /dev/null 2>&1
    else
        # no pty
        echo "NO PTY"
        script -qc "$event_cmd" > /dev/null 2>&1
    fi
}

export LD_LIBRARY_PATH=/usr/local/lib
cp /usr/lib/gstreamer-1.0/libgstlibcamera.so /usr/local/lib/gstreamer-1.0/libgstlibcamera.so
pty_exec
